
package com.querytemplate.query.service ;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import com.querytemplate.query.entitys.Usuario;
import org.springframework.data.repository.query.Param;


public interface UsuarioService{
 
		public Usuario  findByNombre(String nombre);
		public Usuario  findByPaswork(String paswork);
		public List<Usuario>  findByNombreContaining(String nombre);
		public List<Usuario>  findByPasworkContaining(String paswork);
		public Usuario findById(Long id);
		public boolean saveUsuario(Usuario usuario);
		public List<Usuario> getAllUsuario();
		public boolean deleteUsuario(Long id);
		public boolean updateUsuario(Usuario usuario);
 		public boolean saveOrUpdateUsuario(Usuario usuario);
	    public List<Usuario> findAllUser();
 		public void insertUser(Usuario usuario);

}
