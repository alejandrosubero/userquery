
package com.querytemplate.query.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




public class RolPojo implements Serializable {

private static final long serialVersionUID = 3413748601457031524L;

		private Long id;

		private String roles;

		public Long getId() { 
			return id;
		}
		public void setId(Long  id) {
			this.id = id;
		}
		public String getRoles() { 
			return roles;
		}
		public void setRoles(String  roles) {
			this.roles = roles;
		}
			public boolean equalsRolPojo(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
					RolPojo rolpojo = (RolPojo) o;
						return 			Objects.equals(id, rolpojo.id) ||
			Objects.equals(roles, rolpojo.roles);

}}
