
package com.querytemplate.query.entitys;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




@Entity
@Table(name = "rol")
public class Rol implements Serializable {

private static final long serialVersionUID = 8373405350099485642L;

		@Id
		@GeneratedValue(generator = "sequence_mat_id_generator")
		@SequenceGenerator(name="sequence_mat_id_generator", initialValue= 25, allocationSize=1000)
		@Column(name = "id", updatable = true, nullable = false, length = 25)
		private Long id;


		@Column(name = "roles", updatable = true, nullable = true, length = 200)
		private String roles;


		public Long getId() { 
			return id;
		}
		public void setId(Long  id) {
			this.id = id;
		}
		public String getRoles() { 
			return roles;
		}
		public void setRoles(String  roles) {
			this.roles = roles;
		}
			public boolean equalsRol(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
					Rol rol = (Rol) o;
						return 			Objects.equals(id, rol.id) ||
			Objects.equals(roles, rol.roles);

}}
