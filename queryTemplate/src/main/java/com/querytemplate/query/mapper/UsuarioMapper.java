

package com.querytemplate.query.mapper;
import com.querytemplate.query.entitys.Usuario;
import com.querytemplate.query.pojo.UsuarioPojo;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ArrayList;

    @Component
    public class UsuarioMapper {

        public Usuario PojoToEntity(UsuarioPojo pojo) {
           Usuario entity = new Usuario();
          entity.setId(pojo.getId());
          entity.setNombre(pojo.getNombre());
          entity.setPaswork(pojo.getPaswork());
            return entity;
        }


    public UsuarioPojo entityToPojo(Usuario entity) {
        UsuarioPojo pojo = new UsuarioPojo();
        pojo.setId(entity.getId());
        pojo.setNombre(entity.getNombre());
        pojo.setPaswork(entity.getPaswork());

            return pojo;
        }

}
