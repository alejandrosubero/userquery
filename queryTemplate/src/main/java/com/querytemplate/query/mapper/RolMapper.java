

package com.querytemplate.query.mapper;
import com.querytemplate.query.entitys.Rol;
import com.querytemplate.query.pojo.RolPojo;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ArrayList;

    @Component
    public class RolMapper {

        public Rol PojoToEntity(RolPojo pojo) {
           Rol entity = new Rol();
          entity.setId(pojo.getId());
          entity.setRoles(pojo.getRoles());
            return entity;
        }


    public RolPojo entityToPojo(Rol entity) {
        RolPojo pojo = new RolPojo();
        pojo.setId(entity.getId());
        pojo.setRoles(entity.getRoles());

            return pojo;
        }

}
