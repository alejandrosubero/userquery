

package com.querytemplate.query.serviceImplement ;

import com.querytemplate.query.service.RolService;
import com.querytemplate.query.repository.RolRepository;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import com.querytemplate.query.entitys.Rol;




@Service
public class RolServiceImplement implements RolService {

protected static final Log logger = LogFactory.getLog(RolServiceImplement.class);
@Autowired
private RolRepository rolrepository;

		@Override
		public Rol findByRoles(String roles){

		logger.info("Starting getRol");
			Rol rolEntity = new Rol();
		Optional<Rol> fileOptional1 = rolrepository.findByRoles(roles);

		if (fileOptional1.isPresent()) { 

				try {
			rolEntity = fileOptional1.get();
				} catch (DataAccessException e) {
				logger.error(" ERROR : " + e);

				}
		}
		return rolEntity;	}




		@Override
		public List<Rol> getAllRol(){
		logger.info("Get allProyect");
			List<Rol> listaRol = new ArrayList<Rol>();
				rolrepository.findAll().forEach(rol -> listaRol.add(rol));
			return listaRol;
}


		@Override
		public boolean saveRol(Rol rol){
		logger.info("Save Proyect");


				try {
				rolrepository.save(rol);
				return true;
				} catch (DataAccessException e) {
				logger.error(" ERROR : " + e);
				return false;
				}
		}


		@Override
		public boolean deleteRol( Long id){
		logger.info("Delete Proyect");
		boolean clave = false;


				try {
				rolrepository.deleteById(id);
				clave = true;
				} catch (DataAccessException e) {
				logger.error(" ERROR : " + e);
				clave = false;
				}
		return clave;
	}



		@Override
		public boolean updateRol(Rol  rol ){
			logger.info("Update Proyect");
			boolean clave = false;
		Rol empre = findById(rol.getId());
			empre = rol;

				try {
				rolrepository.save(empre);
						clave = true;
				} catch (DataAccessException e) {
				logger.error(" ERROR : " + e);
				clave = false;
				}

					return clave;
	}



		@Override
		public Rol findById( Long id){
				return  rolrepository.findById(id).get();
		}



		@Override
		public boolean saveOrUpdateRol(Rol  rol ){
			logger.info("Update Proyect");
			boolean clave = false;
			logger.info("Starting getEmpresa");
			Optional<Rol> fileOptional2 = rolrepository.findById(rol.getId());
			if (fileOptional2.isPresent()) { 
				clave = this.updateRol(rol);
				logger.info(" is update");
			} else {
					clave = this.saveRol(rol);
					logger.info(" is save");
 				}
 		return clave;
		}


		@Override
		public List<Rol> findByRolesContaining(String roles){
			logger.info("Get allProyect");
 			List<Rol> listaRol = new ArrayList<Rol>();
			listaRol = rolrepository.findByRolesContaining(roles);
  			return listaRol;
		}




}
