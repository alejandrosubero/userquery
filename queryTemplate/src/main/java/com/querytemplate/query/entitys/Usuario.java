
package com.querytemplate.query.entitys;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

private static final long serialVersionUID = -8890736226067905603L;

		@Id
		@GeneratedValue(generator = "sequence_mat_id_generator")
		@SequenceGenerator(name="sequence_mat_id_generator", initialValue= 25, allocationSize=1000)
		@Column(name = "id", updatable = true, nullable = false, length = 25)
		private Long id;


		@Column(name = "nombre", updatable = true, nullable = true, length = 200)
		private String nombre;


		@Column(name = "paswork", updatable = true, nullable = true, length = 200)
		private String paswork;


		public Long getId() { 
			return id;
		}
		public void setId(Long  id) {
			this.id = id;
		}
		public String getNombre() { 
			return nombre;
		}
		public void setNombre(String  nombre) {
			this.nombre = nombre;
		}
		public String getPaswork() { 
			return paswork;
		}
		public void setPaswork(String  paswork) {
			this.paswork = paswork;
		}
			public boolean equalsUsuario(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
					Usuario usuario = (Usuario) o;
						return 			Objects.equals(id, usuario.id) ||
			Objects.equals(nombre, usuario.nombre) ||
			Objects.equals(paswork, usuario.paswork);

}}
