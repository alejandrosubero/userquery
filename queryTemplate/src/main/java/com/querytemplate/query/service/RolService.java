
package com.querytemplate.query.service ;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import com.querytemplate.query.entitys.Rol;


public interface RolService{
 
		public Rol  findByRoles(String roles);

		public List<Rol>  findByRolesContaining(String roles);

		public Rol findById(Long id);
		public boolean saveRol(Rol rol);
		public List<Rol> getAllRol();
		public boolean deleteRol(Long id);
		public boolean updateRol(Rol rol);
 		public boolean saveOrUpdateRol(Rol rol);

}
