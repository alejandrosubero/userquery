

package com.querytemplate.query.controller;
import com.querytemplate.query.entitys.Usuario;
import com.querytemplate.query.validation.UsuarioValidation;
import com.querytemplate.query.mapper.UsuarioMapper;
import com.querytemplate.query.service.UsuarioService;
import com.querytemplate.query.mapper.MapperEntityRespone;
import com.querytemplate.query.pojo.EntityRespone;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.querytemplate.query.pojo.UsuarioPojo;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    UsuarioValidation usuarioValidationService;

    @Autowired
   UsuarioMapper usuarioMapper;

    @Autowired
   MapperEntityRespone mapperEntityRespone;



        @GetMapping("/Getnombre/{nombre}")
        private  ResponseEntity<EntityRespone> findByNombre(@PathVariable("nombre") String  nombre) {
        String busca = (String) usuarioValidationService.validation(nombre);
        try {
                EntityRespone entityRespone = mapperEntityRespone.setEntityTobj(usuarioService.findByNombre(busca));
                return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
             } catch (DataAccessException e) {
                 EntityRespone entityRespone = mapperEntityRespone.setEntityResponT(null, "Ocurrio un error", e.getMessage());
             return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.BAD_REQUEST);
        }
     }

        @GetMapping("/Getpaswork/{paswork}")
        private  ResponseEntity<EntityRespone> findByPaswork(@PathVariable("paswork") String  paswork) {
        String busca = (String) usuarioValidationService.validation(paswork);
        try {
                EntityRespone entityRespone = mapperEntityRespone.setEntityTobj(usuarioService.findByPaswork(busca));
                return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
             } catch (DataAccessException e) {
                 EntityRespone entityRespone = mapperEntityRespone.setEntityResponT(null, "Ocurrio un error", e.getMessage());
             return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.BAD_REQUEST);
        }
     }

        @GetMapping("/Getnombrecontain/{nombre}")
        private ResponseEntity<EntityRespone> findByNombreContain(@PathVariable("nombre") String  nombre) {
              String busca = (String) usuarioValidationService.validation(nombre);
              EntityRespone entityRespone = mapperEntityRespone.setEntityT(usuarioService.findByNombreContaining(busca));
              return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
        }

        @GetMapping("/Getpasworkcontain/{paswork}")
        private ResponseEntity<EntityRespone> findByPasworkContain(@PathVariable("paswork") String  paswork) {
              String busca = (String) usuarioValidationService.validation(paswork);
              EntityRespone entityRespone = mapperEntityRespone.setEntityT(usuarioService.findByPasworkContaining(busca));
              return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
        }


        @GetMapping("/GetUsuario/{id}")
          private ResponseEntity<EntityRespone> findById(@PathVariable("id") String id) {
          EntityRespone entityRespone = mapperEntityRespone.setEntityTobj(usuarioService.findById(usuarioValidationService.valida_id(id))); 
             return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
          }


        @GetMapping("/GetAllUsuario")
        private  ResponseEntity<EntityRespone> getAllUsuario(){
        EntityRespone entityRespone = mapperEntityRespone.setEntityT(usuarioService.getAllUsuario());
            return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK); }




        @PostMapping("/save")
        private Boolean  saveUsuario(@RequestBody UsuarioPojo  usuario){ 
            return usuarioService.saveUsuario(usuarioMapper.PojoToEntity(usuarioValidationService.valida(usuario)) ); }



        @PostMapping("/Update")
        private Long UpdateUsuario(@RequestBody UsuarioPojo  usuario){ 
        usuarioService.updateUsuario(usuarioMapper.PojoToEntity(usuarioValidationService.valida(usuario)));
            return usuario.getId(); }


        @PostMapping("/saveOrUpdate")
        private boolean saveOrUpdateUsuario(@RequestBody UsuarioPojo  usuario){ 
            return usuarioService.saveOrUpdateUsuario(usuarioMapper.PojoToEntity(usuarioValidationService.valida(usuario)) ); }


        @DeleteMapping("/deleteUsuario/{id}")
            private boolean deleteUsuario(@PathVariable("id") String id) {
            return usuarioService.deleteUsuario(usuarioValidationService.valida_id(id)); }


    @PostMapping("/saveUSER")
    private Boolean  saveUser(@RequestBody UsuarioPojo  usuario){
        boolean valor = false;
        try {
            usuarioService.insertUser(usuarioMapper.PojoToEntity(usuarioValidationService.valida(usuario)) );
            valor = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return valor;
    }


    @GetMapping("/GetAllUserss")
    private  ResponseEntity<EntityRespone> getAllUserss(){
        EntityRespone entityRespone = mapperEntityRespone.setEntityT(usuarioService.findAllUser());
        return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
        }



}
