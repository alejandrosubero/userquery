package com.querytemplate.query;

import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.commons.logging.Log;



@SpringBootApplication
public class queryTemplateApplication {

		protected static final Log logger = LogFactory.getLog(queryTemplateApplication.class);
		public static void main(String[] args) {
		logger.info("the document  Swagger is in link: ==>  http://localhost:1111/querytemplate/swagger-ui.html");
			SpringApplication.run(queryTemplateApplication.class, args);
		logger.info("the document  Swagger is in link: ==>  http://localhost:1111/querytemplate/swagger-ui.html");
	}

}

