

package com.querytemplate.query.controller;
import com.querytemplate.query.entitys.Rol;
import com.querytemplate.query.validation.RolValidation;
import com.querytemplate.query.mapper.RolMapper;
import com.querytemplate.query.service.RolService;
import com.querytemplate.query.mapper.MapperEntityRespone;
import com.querytemplate.query.pojo.EntityRespone;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.querytemplate.query.pojo.RolPojo;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rol")
public class RolController {

    @Autowired
    RolService rolService;

    @Autowired
    RolValidation rolValidationService;

    @Autowired
   RolMapper rolMapper;

    @Autowired
   MapperEntityRespone mapperEntityRespone;



        @GetMapping("/Getroles/{roles}")
        private  ResponseEntity<EntityRespone> findByRoles(@PathVariable("roles") String  roles) {
        String busca = (String) rolValidationService.validation(roles);
        try {
                EntityRespone entityRespone = mapperEntityRespone.setEntityTobj(rolService.findByRoles(busca));
                return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
             } catch (DataAccessException e) {
                 EntityRespone entityRespone = mapperEntityRespone.setEntityResponT(null, "Ocurrio un error", e.getMessage());
             return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.BAD_REQUEST);
        }
     }

        @GetMapping("/Getrolescontain/{roles}")
        private ResponseEntity<EntityRespone> findByRolesContain(@PathVariable("roles") String  roles) {
              String busca = (String) rolValidationService.validation(roles);
              EntityRespone entityRespone = mapperEntityRespone.setEntityT(rolService.findByRolesContaining(busca));
              return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
        }


        @GetMapping("/GetRol/{id}")
          private ResponseEntity<EntityRespone> findById(@PathVariable("id") String id) {
          EntityRespone entityRespone = mapperEntityRespone.setEntityTobj(rolService.findById(rolValidationService.valida_id(id))); 
             return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK);
          }


        @GetMapping("/GetAllRol")
        private  ResponseEntity<EntityRespone> getAllRol(){
        EntityRespone entityRespone = mapperEntityRespone.setEntityT(rolService.getAllRol());
            return new ResponseEntity<EntityRespone>(entityRespone, HttpStatus.OK); }



        @PostMapping("/save")
        private Boolean  saveRol(@RequestBody RolPojo  rol){ 
            return rolService.saveRol(rolMapper.PojoToEntity(rolValidationService.valida(rol)) ); }



        @PostMapping("/Update")
        private Long UpdateRol(@RequestBody RolPojo  rol){ 
        rolService.updateRol(rolMapper.PojoToEntity(rolValidationService.valida(rol)));
            return rol.getId(); }


        @PostMapping("/saveOrUpdate")
        private boolean saveOrUpdateRol(@RequestBody RolPojo  rol){ 
            return rolService.saveOrUpdateRol(rolMapper.PojoToEntity(rolValidationService.valida(rol)) ); }


        @DeleteMapping("/deleteRol/{id}")
            private boolean deleteRol(@PathVariable("id") String id) {
            return rolService.deleteRol(rolValidationService.valida_id(id)); }

}
