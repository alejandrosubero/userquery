

package com.querytemplate.query.serviceImplement;

import com.querytemplate.query.entitys.Usuario;
import com.querytemplate.query.repository.UsuarioRepository;
import com.querytemplate.query.service.UsuarioService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class UsuarioServiceImplement implements UsuarioService {

    protected static final Log logger = LogFactory.getLog(UsuarioServiceImplement.class);
    @Autowired
    private UsuarioRepository usuariorepository;

    @Override
    public Usuario findByNombre(String nombre) {
        logger.info("Starting getUsuario");
        Usuario usuarioEntity = new Usuario();
        Optional<Usuario> fileOptional1 = usuariorepository.findByNombre(nombre);
        if (fileOptional1.isPresent()) {
            try {
                usuarioEntity = fileOptional1.get();
            } catch (DataAccessException e) {
                logger.error(" ERROR : " + e);
            }
        }
        return usuarioEntity;
    }

    @Override
    public Usuario findByPaswork(String paswork) {
        logger.info("Starting getUsuario");
        Usuario usuarioEntity = new Usuario();
        Optional<Usuario> fileOptional1 = usuariorepository.findByPaswork(paswork);
        if (fileOptional1.isPresent()) {
            try {
                usuarioEntity = fileOptional1.get();
            } catch (DataAccessException e) {
                logger.error(" ERROR : " + e);
            }
        }
        return usuarioEntity;
    }


    @Override
    public List<Usuario> getAllUsuario() {
        logger.info("Get allProyect");
        List<Usuario> listaUsuario = new ArrayList<Usuario>();
        usuariorepository.findAll().forEach(usuario -> listaUsuario.add(usuario));
        return listaUsuario;
    }


    @Override
    public boolean saveUsuario(Usuario usuario) {
        logger.info("Save Proyect");
        try {
            usuariorepository.save(usuario);
            return true;
        } catch (DataAccessException e) {
            logger.error(" ERROR : " + e);
            return false;
        }
    }


    @Override
    public boolean deleteUsuario(Long id) {
        logger.info("Delete Proyect");
        boolean clave = false;
        try {
            usuariorepository.deleteById(id);
            clave = true;
        } catch (DataAccessException e) {
            logger.error(" ERROR : " + e);
            clave = false;
        }
        return clave;
    }


    @Override
    public boolean updateUsuario(Usuario usuario) {
        logger.info("Update Proyect");
        boolean clave = false;
        Usuario empre = findById(usuario.getId());
        empre = usuario;
        try {
            usuariorepository.save(empre);
            clave = true;
        } catch (DataAccessException e) {
            logger.error(" ERROR : " + e);
            clave = false;
        }
        return clave;
    }


    @Override
    public Usuario findById(Long id) {
        return usuariorepository.findById(id).get();
    }


    @Override
    public boolean saveOrUpdateUsuario(Usuario usuario) {
        logger.info("Update Proyect");
        boolean clave = false;
        logger.info("Starting getEmpresa");
        Optional<Usuario> fileOptional2 = usuariorepository.findById(usuario.getId());
        if (fileOptional2.isPresent()) {
            clave = this.updateUsuario(usuario);
            logger.info(" is update");
        } else {
            clave = this.saveUsuario(usuario);
            logger.info(" is save");
        }
        return clave;
    }


	@Override
    public List<Usuario> findByNombreContaining(String nombre) {
        logger.info("Get allProyect");
        List<Usuario> listaUsuario = new ArrayList<Usuario>();
        listaUsuario = usuariorepository.findByNombreContaining(nombre);
        return listaUsuario;
    }

    @Override
    public List<Usuario> findByPasworkContaining(String paswork) {
        logger.info("Get allProyect");
        List<Usuario> listaUsuario = new ArrayList<Usuario>();
        listaUsuario = usuariorepository.findByPasworkContaining(paswork);
        return listaUsuario;
    }

	@Override
	public List<Usuario> findAllUser() {
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		listaUsuario = usuariorepository.findAllUser();
		return listaUsuario;
	}

	@Override
	public void insertUser(Usuario usuario) {
        UUID uuid = UUID.randomUUID();
		usuariorepository.insertUser(uuid, usuario.getNombre(), usuario.getPaswork());
	}

}
