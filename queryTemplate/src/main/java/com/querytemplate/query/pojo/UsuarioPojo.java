
package com.querytemplate.query.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




public class UsuarioPojo implements Serializable {

private static final long serialVersionUID = 6456191399305531373L;

		private Long id;

		private String nombre;

		private String paswork;

		public Long getId() { 
			return id;
		}
		public void setId(Long  id) {
			this.id = id;
		}
		public String getNombre() { 
			return nombre;
		}
		public void setNombre(String  nombre) {
			this.nombre = nombre;
		}
		public String getPaswork() { 
			return paswork;
		}
		public void setPaswork(String  paswork) {
			this.paswork = paswork;
		}
			public boolean equalsUsuarioPojo(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
					UsuarioPojo usuariopojo = (UsuarioPojo) o;
						return 			Objects.equals(id, usuariopojo.id) ||
			Objects.equals(nombre, usuariopojo.nombre) ||
			Objects.equals(paswork, usuariopojo.paswork);

}}
