
package com.querytemplate.query.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.querytemplate.query.entitys.Usuario;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UsuarioRepository extends CrudRepository< Usuario, Long> {
 
		public Optional<Usuario> findByNombre(String nombre);
		public List<Usuario> findByNombreContaining(String nombre);
		public Optional<Usuario> findByPaswork(String paswork);
		public List<Usuario> findByPasworkContaining(String paswork);

	@Query(value = "SELECT * FROM usuario;", nativeQuery = true)
	public List<Usuario> findAllUser();

	//@Transactional(readOnly = false, isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	@Transactional
	@Modifying
	@Query( value =	"INSERT INTO usuario (id, nombre, paswork) VALUES ( :id, :nombre, :paswork);", nativeQuery = true)
	void insertUser(@Param("id") UUID id , @Param("nombre") String nombre, @Param("paswork") String paswork);
}
