
package com.querytemplate.query.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import com.querytemplate.query.entitys.Rol;

public interface RolRepository extends CrudRepository< Rol, Long> {
		public Optional<Rol> findByRoles(String roles);
		public List<Rol> findByRolesContaining(String roles);
}
