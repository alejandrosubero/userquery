package com.querytemplate.query.pojo;

import java.io.Serializable;
import java.util.List;


public class EntityRespone implements Serializable {
private static final long serialVersionUID = -6570688722342209460L;


    private String error;
   private String mensaje;
   private List<Object> entidades;


 public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    public String getMensaje() {
        return mensaje;
    }


    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public List<Object> getEntidades() {
        return entidades;
    }


    public void setEntidades(List<Object> entidades) {
        this.entidades = entidades;
    }
}